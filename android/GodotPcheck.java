package org.godotengine.godot;

import android.app.Activity;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.content.pm.PackageManager;

public class GodotPcheck extends Godot.SingletonBase
{
	private Activity activity = null; // The main activity of the game

	public boolean check(String packagename)
	{
		Context c = activity.getApplicationContext();
		PackageManager pm = c.getPackageManager();
		try {
			pm.getPackageInfo(packagename, 0);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

 	static public Godot.SingletonBase initialize(Activity activity)
 	{
		return new GodotPcheck(activity);
 	}

	/**
	 * Constructor
	 * @param Activity Main activity
	 */
	public GodotPcheck(Activity activity) {
		registerClass("GodotPcheck", new String[] {
			"check"
		});
		this.activity = activity;
	}
}
